package universo;

import java.util.Random;

public class Villano implements Carta {
    private String nombre="";
    private int ataque=0;
    private int defensa=0;
    private String[] nombres={"Loki","Ultron","Nebula","Thanos","Hela","Magneto"};

    public void generarCarta(){
        //Nombre
        Random ran = new Random();
        int heroe = ran.nextInt(nombres.length);
        nombre=nombres[heroe];
        //Ataque
        int ataque = ran.nextInt(10);
        this.ataque=ataque+1;
        //Defensa
        int defensa = ran.nextInt(10);
        this.defensa=defensa+1;
    }
    public String dameNombre(){
        return this.nombre;
    }
    public int dameAtaque() {
        return this.ataque;
    }

    public int dameDefensa() {
        return this.defensa;
    }
}

