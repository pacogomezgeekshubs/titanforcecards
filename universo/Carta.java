package universo;

public interface Carta {

    public void generarCarta();
    public String dameNombre();
    public int dameAtaque();
    public int dameDefensa();
}
