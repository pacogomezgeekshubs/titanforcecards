package juego;

import universo.Carta;
import universo.Heroe;

import java.util.ArrayList;
import java.util.Iterator;

public abstract class Jugador {
    private int puntosVida=0;
    public ArrayList<Carta> mazo;

    public int getPuntosVida() {
        return puntosVida;
    }

    public void setPuntosVida(int puntosVida) {
        this.puntosVida = puntosVida;
    }

    public abstract void quitarVida(int danyo);
    public abstract void generarMazo();

    public String toString(){
        String visualizarMazo="";
        Iterator it=mazo.iterator();
        while (it.hasNext()){
            visualizarMazo=visualizarMazo+","+it.next();
        }
        return visualizarMazo;
    }
}
